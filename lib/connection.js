/**
 * Module dependencies
 */

var async = require("async"),
  MongoClient = require("mongodb").MongoClient;

/**
 * Manage a connection to a Mongo Server
 *
 * @param {Object} config
 * @return {Object}
 * @api private
 */

var Connection = (module.exports = function Connection(config, cb) {
  var self = this;

  // Hold the config object
  this.config = config || {};
  this.client = null;

  // Build Database connection
  this._buildConnection(function (err, client) {
    if (err) return cb(err);
    if (!client) return cb(new Error("no client object"));

    // Store the DB object
    self.db = client.db(config.database);

    // Return the connection
    cb(null, self);
  });
});

/////////////////////////////////////////////////////////////////////////////////
// PUBLIC METHODS
/////////////////////////////////////////////////////////////////////////////////

/**
 * Create A Collection
 *
 * @param {String} name
 * @param {Object} collection
 * @param {Function} callback
 * @api public
 */

Connection.prototype.createCollection = function createCollection(
  name,
  collection,
  cb
) {
  var self = this;

  // Create the Collection
  this.db.createCollection(name, function (err, result) {
    if (err) return cb(err);

    // Create Indexes
    self._ensureIndexes(result, collection.indexes, cb);
  });
};

/**
 * Drop A Collection
 *
 * @param {String} name
 * @param {Function} callback
 * @api public
 */

Connection.prototype.dropCollection = function dropCollection(name, cb) {
  this.db.dropCollection(name, cb);
};

/////////////////////////////////////////////////////////////////////////////////
// PRIVATE METHODS
/////////////////////////////////////////////////////////////////////////////////

/**
 * Build Server and Database Connection Objects
 *
 * @param {Function} callback
 * @api private
 */

Connection.prototype._buildConnection = function _buildConnection(cb) {
  // Build A Mongo Connection String
  var connectionString = "mongodb://";
  var connectionOptions = { useNewUrlParser: true, useUnifiedTopology: true };

  // Use config connection string if available
  if (this.config.url) connectionString = this.config.url;
  if (this.config.connectionOptions) connectionOptions = this.config.connectionOptions;

  // Open a Connection
  this.client = new MongoClient(connectionString, connectionOptions);
  return this.client.connect(cb);
};

/**
 * Ensure Indexes
 *
 * @param {String} collection
 * @param {Array} indexes
 * @param {Function} callback
 * @api private
 */

Connection.prototype._ensureIndexes = function _ensureIndexes(
  collection,
  indexes,
  cb
) {
  var self = this;

  function createIndex(item, next) {
    collection.ensureIndex(item.index, item.options, next);
  }

  async.each(indexes, createIndex, cb);
};
